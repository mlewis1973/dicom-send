# Dockerfile for dicom-send gear

FROM flywheel/python:main.66a70563

# Python & # DCMTK setup
RUN apt-get update && \
    apt-get install -y --no-install-recommends dcmtk=3.6.5-1 && \
    rm -rf /var/lib/apt/lists/*

# Flywheel spec (v0)
ENV FLYWHEEL /flywheel/v0
WORKDIR ${FLYWHEEL}

# Copy gear specific scripts
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev --no-root
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_dicom_send ${FLYWHEEL}/fw_gear_dicom_send
RUN poetry install --no-dev

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
