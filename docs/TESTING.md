
# Local Testing

Below are instructions for testing the functionality of this Gear locally.

## Setup a DICOM server

Orthanc is an open-source, lightweight server. To download visit [Orthanc
Server](https://www.osimis.io/en/download.html).

After downloading and setup, you can open a browser to visualize the DICOM exchange, via
<http://localhost:8042/app/explorer.html>

- Once Orthanc is up and running, any imaging modality can send instances to Orthanc
  through the DICOM protocol (with the C-Store command). For example:

```bash
storescu -aec ORTHANC localhost 4242 *.dcm1
```

- Orthanc can act both as a C-Store client (SCU) or as a C-Store server (SCP). In other
  words, it can either send or receive DICOM files. In the case of this Gear, we setup
  the server as the receiver. For more information, see the [Orthanc Server
  Book](https://book.orthanc-server.com/index.html).

- **NOTE:** You may need to set up port forwarding on your router so that the DICOM port
  (default '4242') is routed to your machine's IP address.  

### TLS mode

You can start the server in TLS mode by running `./tests/orthanc/start.sh` but only _if
you have the server binary in your path_ and only _if you run macOSX_.  It works on
linux too but the config for TLS is only provided for Mac OSX.

Setup taken from the [Orthanc Docs](https://book.orthanc-server.com/faq/dicom-tls.html)

## Build the Image

To build the image:

```bash
git clone https://github.com/flywheel-apps/dicom-send.git
cd dicom-send
fw-beta gear build
```

### Run the Image Locally

Set config and input values with
[fw-beta](https://flywheel-io.gitlab.io/tools/app/cli/fw-beta/gear/config/)

#### Set config values

- port = 4242
- tls_enabled=enabled
- called_ae=orthanc

#### Set inputs

Test file of your own

- file = /tmp/dicom/test1.dcm

Certificate files found in `./tests/orthanc`

- add_cert_file = ./tests/orthanc/orthanc.crt (provided)
- key = ./tests/orthanc/dcmtk.key  (Provided)
- cert = ./tests/orthanc/dcmtk.crt (Provided)

## Testing against a real PACS
<!-- markdownlint-disable ol-prefix -->

Flywheel internal:  Not sensitive information, but the following steps will
only work for Flywheel employees.  Can be applied anywhere though.

1. ssh to `scien-dev1`
2. Enable orthanc server (Nate has already installed and configured it): `sudo
   systemctl enable orthanc.service`
3. Find listening port, and current IP of VM:

<!-- markdownlint-disable line-length -->
```bash
naterichman:orthanc/ $ sudo ss -atrpn | grep Orthanc
LISTEN  0   128     0.0.0.0:8042    0.0.0.0:*   users:(("Orthanc",pid=15588,fd=7))
LISTEN  0   50      0.0.0.0:4242    0.0.0.0:*   users:(("Orthanc",pid=15588,fd=6))
naterichman:orthanc/ $ curl ifconfig.me
35.222.175.5% 
```

4. Ensure ports are open using iptables:

```bash
naterichman:orthanc/ $ sudo iptables -I INPUT -p tcp --dport 4242 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

naterichman:orthanc/ $ sudo iptables -I OUTPUT -p tcp --dport 4242 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
```
<!-- markdownlint-enable line-length -->

5. Ensure firewall rule exists for allowing connections to VM:
    a. Add a network tag "Orthanc" to the VM in google compute engine
    b. Go to VPC network app in google cloud
    c. Ensure rule "orthanc-to-scien-dev" exists, if it does, youre good, if
    not continue
    d. Use create firewall rule to create an ingress rule to the network tag
    "Orthanc" on port 4242 from any ip, `0.0.0.0/0`
6. Ensure you can connect using `nmap` if you have it installed:

```bash
➜  /work $ nmap 35.222.175.5 -Pn -p 4242
Starting Nmap 7.80 ( https://nmap.org ) at 2022-04-11 14:26 CDT
Nmap scan report for 5.175.222.35.bc.googleusercontent.com (35.222.175.5)
Host is up (0.019s latency).

PORT     STATE SERVICE
4242/tcp open  vrml-multi-use

Nmap done: 1 IP address (1 host up) scanned in 1.29 seconds
```

This will say `open` if it's done right, or `filtered` otherwise.

7. You can follow logs from orthanc with `sudo journalctl --follow
   --unit=orthanc.service`
   or `tail -f /var/log/orthanc/Orthanc.log`

8. Cleanup
    a. Disable firewall rule in GCP (only if you want)
    b. Turn off Orthanc `sudo systemctl stop orthanc.service`
    c. Remove iptables rules.

## Using a different dicom server

Orthanc seems to not work sometimes for TLS connections, I've also had success using
dcmtk's `storescp`:

you can set log level to trace and pass in the same AET and port (must turn off orthanc
first)

```bash
sudo storescp -ll trace -pm +xw -aet ORTHANC \
    +tls ./tests/orthanc/orthanc.key ./tests/orthanc/orthanc/orthanc.crt \
    +cf ./tests/orthanc/dcmtk.crt 4242
```

<!-- markdownlint-enable ol-prefix -->
