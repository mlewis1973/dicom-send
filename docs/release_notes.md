<!-- markdownlint-disable no-emphasis-as-header -->
# Release notes

# 4.0.0

__Enhancements__

* Rewrite the gear using `pynetdicom`:
  * Better logging
  * Better errors
  * Better performance due to bulk associations

__Maintenance__

* Full linting, typing, vulnerability changing
* Much improved testing (30% cc -> 90%)

# 3.0.2

__Enhancements__

* Transmit dicoms in ProcessPoolExecutor for performance improvement.

## 3.0.0

__Enhancements__

* Expose TLS options from storescu, TLS, anonymous TLS, key/cert files, and
  configuration values.
* Improve logging of storescu errors
* Add retry when downloading files (no file input)

__Maintenance__

* Reorganize code base
* Change from SDK -> CoreClient
* Change from pydicom 2.1 -> 2.3 and fw-file
* Add linting, typing, etc. from flywheel qa-ci
* Add tests
