import logging
import tarfile
import zipfile
from contextlib import nullcontext as does_not_raise
from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_send.main import (
    dicom_send,
    dicom_send_file,
    dicom_send_session,
    download_file,
    get_dicom_collection,
)

from .conftest import entry


def files(empty=False):
    files = []
    if empty:
        x = MagicMock()
        x.size = 0
        x.file_size = 0
        return [x]
    for i in range(6):
        x = MagicMock()
        x.size = i
        x.file_size = i
        files.append(x)
    return files


@pytest.mark.parametrize(
    "valid, empty",
    [
        (True, True),
        (True, False),
        (False, True),
    ],
)
def test_get_dicom_collection_from_zip(mocker, valid, empty, caplog):
    is_tar = mocker.patch("fw_gear_dicom_send.main.tarfile.is_tarfile")
    is_zip = mocker.patch("fw_gear_dicom_send.main.zipfile.is_zipfile")
    zipf = mocker.patch("fw_gear_dicom_send.main.zipfile.ZipFile")
    zipobj = zipf.return_value.__enter__.return_value
    is_zip.return_value = True
    is_tar.return_value = False
    if valid:
        zipobj.filelist = files(empty)
    else:
        zipf.return_value.__enter__.side_effect = zipfile.BadZipFile("bad zip")
    infile = MagicMock()
    raises = pytest.raises(SystemExit) if (not valid or empty) else does_not_raise()
    with raises, caplog.at_level(logging.INFO):
        coll = get_dicom_collection(infile)
    if not valid:
        assert "is not valid" in caplog.record_tuples[-1][2]
    elif empty:
        assert "is empty. Exiting" in caplog.record_tuples[-1][2]
    else:
        zipobj.extractall.assert_called_once()
        assert coll.is_tmp


@pytest.mark.parametrize(
    "valid, empty",
    [
        (True, True),
        (True, False),
        (False, True),
    ],
)
def test_get_dicom_collection_from_tar(mocker, valid, empty, caplog):
    is_tar = mocker.patch("fw_gear_dicom_send.main.tarfile.is_tarfile")
    is_zip = mocker.patch("fw_gear_dicom_send.main.zipfile.is_zipfile")
    tarf = mocker.patch("fw_gear_dicom_send.main.tarfile.open")
    tarobj = tarf.return_value.__enter__.return_value
    is_tar.return_value = True
    is_zip.return_value = False
    if valid:
        tarobj.getmembers.return_value = files(empty)
    else:
        tarf.return_value.__enter__.side_effect = tarfile.ReadError("bad tar")
    infile = MagicMock()
    raises = pytest.raises(SystemExit) if (not valid or empty) else does_not_raise()
    with raises, caplog.at_level(logging.INFO):
        coll = get_dicom_collection(infile)
    if not valid:
        assert "is not valid" in caplog.record_tuples[-1][2]
    elif empty:
        assert "is empty. Exiting" in caplog.record_tuples[-1][2]
    else:
        tarobj.extractall.assert_called_once()
        assert coll.is_tmp


def test_get_dicom_collection_from_single_file(mocker, caplog):
    is_zip = mocker.patch("fw_gear_dicom_send.main.zipfile.is_zipfile")
    is_tar = mocker.patch("fw_gear_dicom_send.main.tarfile.is_tarfile")
    shutil = mocker.patch("fw_gear_dicom_send.main.shutil")
    is_tar.return_value = False
    is_zip.return_value = False
    infile = MagicMock()
    with caplog.at_level(logging.INFO):
        _ = get_dicom_collection(infile)
    shutil.move.assert_called_once()


def test_download_file(api, client, tmp_path):
    file_content = b"hello, world"

    def content():
        return file_content, 200

    api.add_callback("/api/acquisitions/1/files/1", content)
    download_file(client, "1", "1", tmp_path / "1")
    file_ = tmp_path / "1"
    assert file_.exists and file_.is_file()
    assert file_.read_bytes() == file_content


def test_dicom_send(mocker):
    coll = MagicMock()
    ae_config = MagicMock()
    run = mocker.patch("fw_gear_dicom_send.main.sender.run")
    exp = 0, 0
    run.return_value = exp
    res = dicom_send(coll, ae_config)
    assert res == exp
    run.assert_called_once_with(
        coll,
        ae_config,
        group="0x0021",
        identifier="Flywheel",
        tag_value="DICOM Send",
    )


def test_dicom_send_file(api, mocker, client):
    dcms = mocker.patch("fw_gear_dicom_send.main.get_dicom_collection")
    infile = MagicMock()
    infile.name = "infile"
    ae_config = MagicMock()
    send = mocker.patch("fw_gear_dicom_send.main.dicom_send")
    send.return_value = (2, 1)
    api.add_response(
        "/api/acquisitions/1",
        {
            "label": "test-acquisition",
            "parents": {
                "group": "test-group",
                "project": "1",
                "subject": "1",
                "session": "1",
            },
        },
    )
    api.add_response("/api/sessions/1", {"label": "test-session"})
    api.add_response("/api/subjects/1", {"label": "test-subject"})
    api.add_response("/api/projects/1", {"label": "test-project"})

    entries, pres, sent = dicom_send_file(client, infile, ae_config, "1")
    dcms.assert_called_once_with(infile)
    # Ensure that the argument corresponding to "group" in the dicom send method
    # Is actually the default dicom group "0x0221" and not overriden by the group
    # Label -- GEAR-3019
    assert send.call_args_list[0].args[2] == "0x0021"
    assert pres == 2
    assert sent == 1
    assert len(entries) == 1
    assert entries[0] == entry(
        "test-group/test-project/test-subject/test-session/test-acquisition/infile",
        "1",
        "infile",
        2,
        1,
    )


def test_dicom_send_session_no_dicoms(api, mocker, client):
    dcms = mocker.patch("fw_gear_dicom_send.main.get_dicom_collection")
    release = mocker.patch("fw_gear_dicom_send.main.release_association")
    ae_config = MagicMock()
    send = mocker.patch("fw_gear_dicom_send.main.dicom_send")
    api.add_response(
        "/api/sessions/1",
        {
            "label": "test-session",
            "parents": {
                "group": "test-group",
                "project": "1",
                "subject": "1",
            },
        },
    )
    api.add_response("/api/subjects/1", {"label": "test-subject"})
    api.add_response("/api/projects/1", {"label": "test-project"})
    api.add_response("/api/sessions/1/acquisitions", [])
    entries, present, sent = dicom_send_session(client, "1", ae_config)
    assert not entries
    assert present == 0
    assert sent == 0
    dcms.assert_not_called()
    send.assert_not_called()
    release.assert_called_once()


def test_dicom_send_session(api, mocker, caplog, client):
    # pylint: disable=too-many-locals
    ae_config = MagicMock()
    dcms = mocker.patch("fw_gear_dicom_send.main.get_dicom_collection")
    release = mocker.patch("fw_gear_dicom_send.main.release_association")
    send = mocker.patch("fw_gear_dicom_send.main.dicom_send")
    api.add_response(
        "/api/sessions/1",
        {
            "label": "test-session",
            "parents": {
                "group": "test-group",
                "project": "1",
                "subject": "1",
            },
        },
    )
    api.add_response("/api/subjects/1", {"label": "test-subject"})
    api.add_response("/api/projects/1", {"label": "test-project"})
    api.add_response(
        "/api/sessions/1/acquisitions",
        [
            {
                "_id": "1",
                "label": "test-acquisition-1",
                "files": [
                    {"name": "1", "type": "dicom"},
                    {"name": "2", "type": "dicom"},
                ],
            },
            {
                "_id": "2",
                "label": "test-acquisition-2",
                "files": [
                    {"name": "1", "type": "dicom"},
                    {"name": "2", "type": "dicom"},
                    {"name": "3", "type": "nifti"},
                ],
            },
            {
                "_id": "3",
                "label": "test-acquisition-3",
                "files": [
                    {"name": "1", "type": "dicom"},
                ],
            },
        ],
    )

    def content():
        return b"hello", 200

    acq_file = [[1, 1], [1, 2], [2, 1], [2, 2], [3, 1]]
    for acq, file in acq_file:
        api.add_callback(f"/api/acquisitions/{acq}/files/{file}", content)
    api.add_callback("/api/acquisitions/2/files/3", content)
    send = mocker.patch("fw_gear_dicom_send.main.dicom_send")
    send.side_effect = [[3, 1], [4, 4], [5, 0], [1, 1], [0, 0]]
    with caplog.at_level(logging.INFO):
        entries, present, sent = dicom_send_session(client, "1", ae_config)
    exp = [
        entry(
            "test-group/test-project/test-subject/test-session/test-acquisition-1/1",
            "1",
            "1",
            3,
            1,
        ),
        entry(
            "test-group/test-project/test-subject/test-session/test-acquisition-1/2",
            "1",
            "2",
            4,
            4,
        ),
        entry(
            "test-group/test-project/test-subject/test-session/test-acquisition-2/1",
            "2",
            "1",
            5,
            0,
        ),
        entry(
            "test-group/test-project/test-subject/test-session/test-acquisition-2/2",
            "2",
            "2",
            1,
            1,
        ),
        entry(
            "test-group/test-project/test-subject/test-session/test-acquisition-3/1",
            "3",
            "1",
            0,
            0,
        ),
    ]
    # Ensure that the argument corresponding to "group" in the dicom send method
    # Is actually the default dicom group "0x0221" and not overriden by the group
    # Label -- GEAR-3019
    assert all(entry.args[2] == "0x0021" for entry in send.call_args_list)
    assert entries == exp
    assert present == 13
    assert sent == 6
    assert "----- Acquisition: test-acquisition-1 (2 files) -----" in caplog.text
    assert "1: 1/3 dicoms sent." in caplog.text
    assert "2: 4/4 dicoms sent." in caplog.text
    assert "----- Acquisition: test-acquisition-2 (2 files) -----" in caplog.text
    assert "1: 0/5 dicoms sent." in caplog.text
    assert "2: 1/1 dicoms sent." in caplog.text
    assert "----- Acquisition: test-acquisition-3 (1 files) -----" in caplog.text
    assert "1: 0/0 dicoms sent." in caplog.text
    # One call for each file
    assert dcms.call_count == len(acq_file)
    assert send.call_count == len(acq_file)
    release.assert_called_once()
