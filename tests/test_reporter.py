import datetime
import logging
from pathlib import Path

import pytest

from fw_gear_dicom_send import reporter


@pytest.mark.parametrize(
    "in_,out_",
    [
        ("t2*.dcm.zip", "t2star.dcm.zip"),
        ("t2_/-.dcm", "t2_-.dcm"),
    ],
)
def test_get_sanitized_filename(in_, out_):
    assert reporter.get_sanitized_filename(Path(in_)) == Path(out_)


entries = [
    {
        "path": "group/project/subject/session/acquisition/file.dcm",
        "acq_id": "1",
        "file_name": "file.dcm",
        "present": 5,
        "sent": 4,
    },
    {
        "path": "group/project/subject/session/acquisition/file2.dcm.zip",
        "acq_id": "1",
        "file_name": "file2.dcm",
        "present": 100,
        "sent": 100,
    },
]


@pytest.mark.parametrize("err", [False, True])
def test_upload_report_signed(api, mocker, tmp_path, caplog, err, client):
    now = mocker.patch("fw_gear_dicom_send.reporter.datetime")

    now.now.return_value = datetime.datetime(2022, 6, 5, 0, 0, 0, 0)
    report_name = "dicom-send_report-test-ses_test-acq_2022-06-05_00-00-00.csv"
    api.add_response("/api/sessions/1", {"label": "test-ses", "_id": 1})
    api.add_response("/api/acquisitions/2", {"label": "test-acq"})
    api.add_response("/api/config", {"signed_url": True})
    api.add_response(
        "/api/sessions/1/files",
        {
            "ticket": "629d15b0c209273351acc67d",
            "urls": {report_name: f"{api.url}/dummy/upload"},
            "headers": {},
        },
        method="POST",
    )
    api.add_response("/dummy/upload", method="PUT", status=(501 if err else 200))
    with caplog.at_level(logging.INFO):
        res = reporter.upload_report(client, entries, tmp_path, 1, 2)
    report = tmp_path / report_name
    assert report.exists() and report.is_file()
    assert api.request_map["PUT /dummy/upload"]["body"] == report.read_bytes()
    if not err:
        assert api.requests[-1]["params"] == {"ticket": "629d15b0c209273351acc67d"}
        assert caplog.record_tuples[-1][2] == (
            "REPORT_SUMMARY:\n"
            "path                                                        "
            "acq_id     file_name     present     sent    \n"
            "group/project/subject/session/acquisition/file.dcm          "
            "1          file.dcm      5           4       \n"
            "group/project/subject/session/acquisition/file2.dcm.zip     "
            "1          file2.dcm     100         100     \n"
        )
        assert res
    else:
        assert "Could not upload report" in caplog.record_tuples[-1][2]
        assert not res


@pytest.mark.parametrize("err", [False, True])
def test_upload_report_not_signed(api, mocker, tmp_path, caplog, err, client):
    now = mocker.patch("fw_gear_dicom_send.reporter.datetime")

    now.now.return_value = datetime.datetime(2022, 6, 5, 0, 0, 0, 0)
    report_name = "dicom-send_report-test-ses_test-acq_2022-06-05_00-00-00.csv"
    api.add_response("/api/sessions/1", {"label": "test-ses", "_id": 1})
    api.add_response("/api/acquisitions/2", {"label": "test-acq"})
    api.add_response("/api/config", {})
    api.add_response(
        "/api/sessions/1/files", status=(501 if err else 200), method="POST"
    )
    with caplog.at_level(logging.INFO):
        _ = reporter.upload_report(client, entries, tmp_path, 1, 2)
    report = tmp_path / report_name
    assert report.exists() and report.is_file()
    if not err:
        assert api.requests[-1]["files"] == {"file": report.read_bytes()}
    else:
        assert "Could not upload report" in caplog.record_tuples[-1][2]
