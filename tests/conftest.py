"""Configure tests."""

import io
import logging
import typing as t

import pydicom
import pytest
from fw_core_client import CoreClient
from fw_file import dicom

pytest_plugins = ("fw_http_testserver",)
log = logging.getLogger()


# Alias for http_testserver
@pytest.fixture
def api(http_testserver):
    return http_testserver


@pytest.fixture
def client(api):
    return CoreClient(
        api_key=f"{api.url}:user", client_name="dicom-send", client_version="0.1.0"
    )


def entry(path, acq_id, file_name, present, sent):
    return {
        "path": path,
        "acq_id": acq_id,
        "file_name": file_name,
        "present": present,
        "sent": sent,
    }


def merge_dcmdict(custom: dict, default: dict) -> dict:
    """Merge a custom dict onto some defaults."""
    custom = custom or {}
    merged = {}
    for key, value in default.items():
        merged[key] = value
    for key, value in custom.items():
        if value is UNSET:
            merged.pop(key)
        else:
            merged[key] = value
    return merged


def apply_dcmdict(  # pylint: disable=inconsistent-return-statements
    dataset: "pydicom.Dataset", dcmdict: dict
) -> None:
    """Add dataelements to a dataset from the given dcmdict."""
    # pylint: disable=invalid-name
    try:
        dcmdict = dcmdict or {}
        for key, value in dcmdict.items():
            if isinstance(value, (list, tuple)) and len(value) == 2:
                VR_dict = pydicom.datadict.dictionary_VR(key)
                VR, value = value
                if VR == VR_dict:
                    dataset.add_new(key, VR, value)
                else:
                    try:
                        dataset.add_new(key, VR_dict, value)
                    except:  # pylint: disable=bare-except
                        log.error(
                            f"Could not add key {key}, VR {VR_dict}, value {value}"
                        )
            else:
                VR = pydicom.datadict.dictionary_VR(key)
                dataset.add_new(key, VR, value)
    except NameError:
        log.error("Need pydicom to use this fixture.")
        return None


# sentinel value for merge() to skip default_dcmdict keys
UNSET = object()


default_dcmdict = dict(
    SOPClassUID="1.2.840.10008.5.1.4.1.1.4",  # MR Image Storage
    SOPInstanceUID="1.2.3",
    PatientID="test",
    StudyInstanceUID="1",
    SeriesInstanceUID="1.2",
)


def create_ds(**dcmdict) -> t.Optional["pydicom.Dataset"]:
    dataset = pydicom.Dataset()
    apply_dcmdict(dataset, dcmdict)
    return dataset


def create_dcm(file=None, preamble=None, file_meta=None, **dcmdict):
    """Create a dummy dicom with configurable values.

    Args:
        file: Optional filepath
        preamble: Optional byte-string to set as the dataset preamble
        file_meta: Optional `pydicom.dataset.FileMetaDataset()`
        dcmdict: Dictionary with either `{<tag>: <value>}` or:
        .. code-block:: python

            {
                <key>: (<VR>, <value>)
            }
    Returns:
        (fw_file.dicom.DICOM): Dicom

    """
    try:
        dcmdict = merge_dcmdict(dcmdict, default_dcmdict)
        dataset = pydicom.FileDataset(file, create_ds(**dcmdict))
        dataset.preamble = preamble or b"\x00" * 128
        dataset.file_meta = pydicom.dataset.FileMetaDataset()
        apply_dcmdict(dataset.file_meta, file_meta)
        file = file or io.BytesIO()
        pydicom.dcmwrite(file, dataset, write_like_original=bool(file_meta))
        if isinstance(file, io.BytesIO):
            file.seek(0)
        return dicom.DICOM(file)
    except NameError:
        log.error("Need pydicom and fw-file to use this fixture.")
        return None


@pytest.fixture
def dcmcoll(tmp_path):
    """Return DICOMCollection with two files."""

    def _gen(**kwargs):
        for key, val in kwargs.items():
            create_dcm(**val, file=str(tmp_path / (key + ".dcm")))
        return dicom.DICOMCollection.from_dir(tmp_path)

    return _gen
