import os
import ssl
from contextlib import nullcontext as does_not_raise
from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_send.parser import AEConfig, TLSOpt, parse_args, parse_tls_opts


@pytest.mark.parametrize(
    "files, peer, enabled, raises",
    [
        (["key", "cert", "add_cert_file"], True, True, False),
        (["key", "cert"], False, True, False),
        (["key"], False, True, True),
        (["cert", "add_cert_file"], False, True, True),
        (["key"], False, False, False),
    ],
)
def test_tls_opts(tmp_path, files, peer, enabled, raises):
    file_dict = {}
    for file in files:
        f = tmp_path / file
        f.touch()
        file_dict[file] = f

    gc = MagicMock()

    def get_input(name: str):
        res = file_dict.get(name)
        return res if res else None

    gc.get_input_path.side_effect = get_input
    gc.config = {"tls_enabled": enabled, "tls_require_peer_cert": peer}
    with pytest.raises(ValueError) if raises else does_not_raise():
        tls_opt = parse_tls_opts(gc)
    if not raises:
        for file, path in file_dict.items():
            assert getattr(tls_opt, file) == path
        assert tls_opt.enabled == enabled
        assert tls_opt.require_peer_cert == peer


def test_tls_get_ssl_context(tmp_path, mocker):
    ssl_mock = mocker.patch("fw_gear_dicom_send.parser.ssl.create_default_context")
    add_cert_file = tmp_path / "add_cert_file"
    cert = tmp_path / "cert.pem"
    key = tmp_path / "key.pem"
    opts = TLSOpt(enabled=True, cert=cert, key=key, add_cert_file=add_cert_file)
    opts.get_ssl_context()
    ssl_mock.assert_called_once_with(ssl.Purpose.SERVER_AUTH, cafile=add_cert_file)
    assert ssl_mock.return_value.verify_mode == ssl.CERT_REQUIRED
    ssl_mock.return_value.load_cert_chain.assert_called_once_with(
        certfile=cert, keyfile=key
    )


@pytest.mark.parametrize("tls_opts_raise", [True, False])
@pytest.mark.parametrize("in_", [None, "test.dcm"])
def test_parse_args(mocker, in_, tmp_path, api, monkeypatch, tls_opts_raise):
    monkeypatch.delenv("FW_DOWNLOAD_RETRY_TIME", raising=False)
    tls = mocker.patch("fw_gear_dicom_send.parser.parse_tls_opts")
    if tls_opts_raise:
        tls.side_effect = ValueError("test")
    api.add_response("/api/acquisitions/test", {"parents": {"session": "test-session"}})
    context = MagicMock()
    context.config = {
        "called_ae": "called_ae",
        "port": 123,
        "calling_ae": "calling_ae",
        "destination": "10.0.0.1",
        "group": "0x0022",
        "identifier": "Flywheel-dicom-send",
        "tag_value": "sent",
        "file_download_retry_time": "15",
    }
    context.destination = {"id": "test-session"}

    def get_input(name: str):
        if name == "api-key":
            return {"key": f"{api.url}:user"}
        return {"hierarchy": {"id": "test"}, "location": {"name": "test.dcm"}}

    context.get_input.side_effect = get_input
    context.get_input_path.return_value = tmp_path / in_ if in_ else None
    if in_:
        context.get_input_path.return_value.touch()
    with pytest.raises(SystemExit) if tls_opts_raise else does_not_raise():
        (
            _,
            ae_config,
            tag_kwargs,
            session_id,
            infile,
            parent_acq,
        ) = parse_args(context)
    if not tls_opts_raise:
        assert os.environ.get("FW_DOWNLOAD_RETRY_TIME") == "15"

        assert ae_config == AEConfig(
            **{
                "called_ae": "called_ae",
                "port": 123,
                "calling_ae": "calling_ae",
                "destination": "10.0.0.1",
                "tls": tls.return_value,
            }
        )
        assert tag_kwargs == {
            "group": "0x0022",
            "identifier": "Flywheel-dicom-send",
            "tag_value": "sent",
        }
        assert session_id == "test-session"
        if in_:
            assert infile == tmp_path / in_
            assert parent_acq == "test"
        else:
            assert infile is None
            assert parent_acq is None
