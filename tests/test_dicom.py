from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_send.dicom import (
    get_application_entity,
    get_association,
    get_presentation_contexts,
)
from fw_gear_dicom_send.parser import AEConfig


@pytest.mark.parametrize("tls", [None, MagicMock()])
def test_get_application_entity(mocker, tls):
    ae = mocker.patch("fw_gear_dicom_send.dicom.ApplicationEntity")
    ae_config = AEConfig(
        **{
            "destination": "127.0.0.1",
            "called_ae": "dest",
            "port": 123,
            "calling_ae": "src",
            "tls": tls,
        }
    )
    res = get_application_entity(ae_config)
    ae.assert_called_once_with(ae_title=ae_config.calling_ae)
    assert res == ae.return_value


@pytest.mark.parametrize(
    "verification,meta,sops,transfers",
    [
        (
            False,
            [
                {
                    # JPEG 2000 TS, MR image storage
                    "SOPClassUID": "1.2.840.10008.5.1.4.1.1.4",
                    "TransferSyntaxUID": "1.2.840.10008.1.2.4.91",
                },
                # CT Image Storage, Explicit VR Little Endian
                {
                    "SOPClassUID": "1.2.840.10008.5.1.4.1.1.2",
                    "TransferSyntaxUID": "1.2.840.10008.1.2.1",
                },
                # Ultrasound Image Storage
                {"SOPClassUID": "1.2.840.10008.5.1.4.1.1.6.1"},
            ],
            3,
            ["1.2.840.10008.1.2.1", "1.2.840.10008.1.2.4.91", "1.2.840.10008.1.2"],
        ),
        (
            True,
            [
                {
                    # Ultrasound Image Storage
                    "SOPClassUID": "1.2.840.10008.5.1.4.1.1.6.1",
                    "TransferSyntaxUID": "1.2.840.10008.1.2.4.91",
                }
            ],
            2,
            ["1.2.840.10008.1.2.4.91", "1.2.840.10008.1.2"],
        ),
    ],
)
def test_presentation_contexts(dcmcoll, verification, meta, sops, transfers):
    dcms = {f"test{i}": {} for i in range(len(meta))}
    coll = dcmcoll(**dcms)
    for i, dcm in enumerate(coll):
        for key, value in meta[i].items():
            setattr(dcm, key, value)

    pres = get_presentation_contexts(coll, verification)
    assert len(pres) == sops
    for c in pres:
        assert all(v in c.transfer_syntax for v in transfers)


def test_get_pres_context_too_many_sops():
    dcms = MagicMock()
    dcms.bulk_get.side_effect = (["test"], (list(range(129))))
    with pytest.raises(RuntimeError):
        get_presentation_contexts(dcms, True)


def test_get_association_fails(mocker):
    ae = mocker.patch("fw_gear_dicom_send.dicom.ApplicationEntity")
    ae.return_value.associate.return_value.is_established = False
    with pytest.raises(RuntimeError):
        get_association(MagicMock(), MagicMock())


@pytest.mark.parametrize("tls", [None, MagicMock()])
def test_get_association(tls, mocker):
    ae_config = MagicMock()
    dcms = MagicMock()
    ae = mocker.patch("fw_gear_dicom_send.dicom.get_application_entity")
    ae.return_value.associate.return_value.is_established = True
    ae_config.tls = tls
    contexts = mocker.patch("fw_gear_dicom_send.dicom.get_presentation_contexts")
    contexts.return_value = [1] * 10
    get_association(ae_config, dcms)
    if tls:
        ae_config.tls.get_ssl_context.assert_called_once()
        ae.return_value.associate.assert_called_once_with(
            ae_config.destination,
            ae_config.port,
            ae_title=ae_config.called_ae,
            tls_args=(
                ae_config.tls.get_ssl_context.return_value,
                ae_config.destination,
            ),
        )
    else:
        ae.return_value.associate.assert_called_once_with(
            ae_config.destination,
            ae_config.port,
            ae_title=ae_config.called_ae,
        )
