import logging
from unittest.mock import MagicMock

import pydicom
import pytest

from fw_gear_dicom_send import sender

from .conftest import create_dcm

group = 0x0021
identifier = "Flywheel"
tag_value = "DICOM Send"


def test_add_private_tag_int_group():
    ds = create_dcm()
    sender.add_private_tag(ds, group, identifier, tag_value)
    identifier_elem = ds.get((group, 0x0010))
    private_elem = ds.get((group, 0x1000))
    assert identifier_elem == "Flywheel"
    assert private_elem == "DICOM Send"


def test_add_private_tag_str_group():
    ds = create_dcm()
    sender.add_private_tag(ds, "0x0021", identifier, tag_value)
    identifier_elem = ds.get((group, 0x0010))
    private_elem = ds.get((group, 0x1000))
    assert identifier_elem == "Flywheel"
    assert private_elem == "DICOM Send"


def test_tagged_once():
    ds = create_dcm()
    sender.add_private_tag(ds, group, identifier, tag_value)
    priv = ds.dataset.raw.private_block(group, identifier)
    for offset in range(0x01, 0xFF):
        assert not ds.get(priv.get_tag(offset))
    # Make sure dicom is only tagged once
    sender.add_private_tag(ds, group, identifier, tag_value)
    for tag in range(0x0011, 0x00FF):
        assert not ds.get((group, tag))


def test_identifier_but_no_tag():
    # Test when tag isn't there but identifier is
    ds = create_dcm()
    ds.dataset.raw.add_new((0x0021, 0x0010), "LO", "Flywheel")
    sender.add_private_tag(ds, group, identifier, tag_value)
    identifier_elem = ds.get((group, 0x0010))
    private_elem = ds.get((group, 0x1000))
    assert identifier_elem == "Flywheel"
    assert private_elem == "DICOM Send"
    # Tag wasn't set anywhere else in the private block
    for tag in range(0x0011, 0x00FF):
        assert not ds.get((group, tag))


def test_no_space_for_identifier(caplog):
    ds = create_dcm()
    for tag in range(0x0010, 0x00FF + 1):
        ds.dataset.raw.add_new((0x0021, tag), "LO", "Not Flywheel")
    with pytest.raises(SystemExit):
        sender.add_private_tag(ds, group, identifier, tag_value)
    err = caplog.records[-1]
    assert err.levelno == 40
    assert err.message.startswith("No free element")


def no_space_for_tag(caplog):
    ds = create_dcm()
    ds.add_new((0x0021, 0x0010), "LO", "Flywheel")
    for tag in range(0x1000, 0x10FF + 1):
        ds.add_new((0x0021, tag), "LO", "Not DICOM Send")
    with pytest.raises(SystemExit):
        sender.add_private_tag(ds, group, identifier, tag_value)
    err = caplog.records[-1]
    assert err.levelno == 40
    assert err.message.startswith("No free element")


@pytest.mark.parametrize(
    "resp, success",
    [
        (None, False),
        (0x0000, True),
        (0xB000, True),
        (0xB006, True),
        (0xB007, True),
        (0x0117, False),
        (0x0122, False),
        (0x0124, False),
        (0x0210, False),
        (0x0211, False),
        (0x0212, False),
        (0xA902, False),
        (0xC003, False),
        (0xFFFF, False),
    ],
)
def test_sender_dicom(resp, success):
    ds = pydicom.Dataset()
    if resp is not None:
        ds[(0x0000, 0x0900)] = pydicom.dataelem.DataElement(
            (0x0000, 0x0900), "US", resp
        )
    association = MagicMock()
    association.send_c_store.return_value = ds
    dcm = MagicMock()
    res = sender.transmit_dicom_file(dcm, association)
    assert res == success


def test_sender_dicom_value_error():
    association = MagicMock()
    association.send_c_store.side_effect = ValueError("test")
    dcm = MagicMock()
    res = sender.transmit_dicom_file(dcm, association)
    assert not res


@pytest.mark.parametrize(
    "sop_class, sop_instance, transfer_syntax, msg",
    [
        (True, True, True, None),
        (True, True, False, "TransferSyntaxUID"),
        (True, False, False, "SOPInstanceUID, TransferSyntaxUID"),
        (False, False, False, "SOPClassUID, SOPInstanceUID, TransferSyntaxUID"),
    ],
)
def test_run(dcmcoll, mocker, caplog, sop_class, sop_instance, transfer_syntax, msg):
    dcm = dcmcoll(**{"test1": {}})
    if not sop_class:
        del dcm[0]["SOPClassUID"]
    if not sop_instance:
        del dcm[0]["SOPInstanceUID"]
    if not transfer_syntax:
        del dcm[0]["TransferSyntaxUID"]
    ae_config = MagicMock()
    mocker.patch("fw_gear_dicom_send.sender.get_association")
    mocker.patch("fw_gear_dicom_send.sender.add_private_tag")
    mocker.patch("fw_gear_dicom_send.sender.transmit_dicom_file")
    with caplog.at_level(logging.INFO):
        sender.run(dcm, ae_config, "", "", "")
    if msg:
        err = caplog.record_tuples[-1]
        assert err[1] == logging.ERROR
        assert "1 errors across dicoms" in err[2]
        assert msg in err[2]
    else:
        assert len(caplog.record_tuples) == 0


def test_run_cannot_associate(mocker, dcmcoll):
    dcm = dcmcoll(**{"test1": {}})
    ae_config = MagicMock()
    get_association = mocker.patch("fw_gear_dicom_send.sender.get_association")
    get_association.side_effect = RuntimeError
    res = sender.run(dcm, ae_config, "", "", "")
    assert res == (1, 0)


def test_run_out_of_resources(mocker, caplog, dcmcoll):
    dcm = dcmcoll(**{"test1": {}})
    ae_config = MagicMock()
    mocker.patch("fw_gear_dicom_send.sender.get_association")
    mocker.patch("fw_gear_dicom_send.sender.add_private_tag")
    senderter = mocker.patch("fw_gear_dicom_send.sender.transmit_dicom_file")
    senderter.side_effect = sender.OutOfResources()
    with caplog.at_level(logging.INFO):
        sender.run(dcm, ae_config, "", "", "")

    err = caplog.record_tuples[-1]
    assert " Unable to transmit dicom due to out of resources error" in err[2]
