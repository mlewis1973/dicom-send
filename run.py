#!/usr/bin/env python3
"""Main script for dicom-send gear."""

import logging
import sys

import flywheel_gear_toolkit

from fw_gear_dicom_send import parser, reporter
from fw_gear_dicom_send.main import dicom_send_file, dicom_send_session

log = logging.getLogger(__name__)


def main(gear_context):  # pylint: disable=too-many-locals
    """Orchestrate dicom-send gear."""
    log.info("Starting dicom-send gear.")
    # Prepare gear arguments by parsing the gear configuration
    (
        client,
        ae_config,
        tag_kwargs,
        session_id,
        infile,
        parent_acq,
    ) = parser.parse_args(gear_context)
    dcms_present, dcms_sent = 0, 0
    entries = []
    assert session_id
    work = gear_context.work_dir

    # Run dicom-send
    if infile:
        assert parent_acq
        entries, dcms_present, dcms_sent = dicom_send_file(
            client, infile, ae_config, parent_acq, **tag_kwargs
        )
    else:
        entries, dcms_present, dcms_sent = dicom_send_session(
            client, session_id, ae_config, **tag_kwargs
        )

    reporter.upload_report(client, entries, work, session_id, parent_acq)

    # Log number of DICOM files transmitted and exit accordingly
    if dcms_present > 0 and dcms_sent == dcms_present:
        log.info(f"All {dcms_present} DICOM files transmitted.")
        return 0
    if dcms_present == 0:
        log.error(
            "No input DICOM files were found. Please check the input file/session"
        )
        return 1
    if dcms_sent < dcms_present:
        log.error(
            "Not all DICOMS were successfully transmitted "
            f"({dcms_sent}/{dcms_present}). Please check report."
        )
        return 2
    log.error("PANIC: More dicoms sent than were present?? Email support@flywheel.io")
    return -1


if __name__ == "__main__":

    with flywheel_gear_toolkit.GearToolkitContext() as context:
        context.init_logging()
        exit_status = main(context)

    log.info(f"Dicom-send gear exited with status {exit_status}.")
    sys.exit(exit_status)
